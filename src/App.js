import { Link, Route, Routes } from 'react-router-dom';
import PostsProvider from './contexts/postsContext';

import AddPost from './pages/AddPost';
import Dashboard from './pages/Dashboard';
import Posts from './pages/Posts';

function App() {
  return (
    <>
      <nav className="nav nav-fill">
        <Link className="nav-link" to="/">
          Dashboard
        </Link>
        <Link className="nav-link" to="/posts">
          Posts
        </Link>
        <Link className="nav-link" to="/add-post">
          Add Post
        </Link>
      </nav>
      <main className="container mt-3">
        <PostsProvider>
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="/posts" element={<Posts />} />
            <Route path="/add-post" element={<AddPost />} />
          </Routes>
        </PostsProvider>
      </main>
    </>
  );
}

export default App;

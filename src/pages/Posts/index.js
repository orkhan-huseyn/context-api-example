import React, { Suspense } from 'react';

const PostsPage = React.lazy(() => import('./Posts'));

export default function () {
  return (
    <Suspense fallback={<h1>Loading...</h1>}>
      <PostsPage />
    </Suspense>
  );
}

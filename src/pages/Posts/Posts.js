import PostList from '../../components/PostList';
import { usePosts } from '../../contexts/postsContext';

export default function Posts() {
  const [posts, , deletePost] = usePosts();

  function handleDelete(index) {
    deletePost(index);
  }

  return (
    <>
      <h2>Posts</h2>
      <PostList list={posts} onDelete={handleDelete} />
    </>
  );
}

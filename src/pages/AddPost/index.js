import React, { Suspense } from 'react';

const AddPostsPage = React.lazy(() => import('./AddPost'));

export default function () {
  return (
    <Suspense fallback={<h1>Loading...</h1>}>
      <AddPostsPage />
    </Suspense>
  );
}

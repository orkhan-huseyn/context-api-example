import { useNavigate } from 'react-router-dom';
import AddPostForm from '../../components/AddPostForm';
import { usePosts } from '../../contexts/postsContext';

export default function AddPost() {
  const [, addPost] = usePosts();
  const navigate = useNavigate();

  function handleSubmit(post) {
    addPost(post);
    navigate('/posts');
  }

  return (
    <>
      <h2>Add post</h2>
      <AddPostForm onSubmit={handleSubmit} />
    </>
  );
}

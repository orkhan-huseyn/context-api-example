import React, { Suspense } from "react";

const DashboardPage = React.lazy(() => import("./Dashboard"));

export default function () {
  return (
    <Suspense fallback={<h1>Loading...</h1>}>
      <DashboardPage />
    </Suspense>
  );
}

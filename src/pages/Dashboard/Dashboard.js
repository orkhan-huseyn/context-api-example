import { usePosts } from '../../contexts/postsContext';

export default function Dashboard() {
  const [posts] = usePosts();

  return (
    <>
      <h1>Welcome, Filankes</h1>
      <p>You have total {posts.length} posts.</p>
    </>
  );
}

import { createContext, useContext, useState } from 'react';

const PostsContext = createContext();

export function usePosts() {
  return useContext(PostsContext);
}

export default function PostsProvider({ children }) {
  const [posts, setPosts] = useState([]);

  function addPost(post) {
    setPosts((posts) => [...posts, post]);
  }

  function deletePost(index) {
    const copyOfPosts = posts.slice();
    copyOfPosts.splice(index, 1);
    setPosts(copyOfPosts);
  }

  return (
    <PostsContext.Provider value={[posts, addPost, deletePost]}>
      {children}
    </PostsContext.Provider>
  );
}

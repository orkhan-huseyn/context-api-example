export default function PostList({ list, onDelete }) {
  return (
    <div className="list-group w-50 mx-auto mt-5">
      {list.map((post, index) => (
        <div key={index} className="list-group-item ">
          <div className="d-flex w-100 justify-content-between">
            <h5 className="mb-1">{post.title}</h5>
            <button
              type="button"
              className="btn btn-link btn-sm"
              onClick={() => onDelete(index)}
            >
              Delete post
            </button>
          </div>
          <p className="mb-1">{post.content}</p>
        </div>
      ))}
    </div>
  );
}

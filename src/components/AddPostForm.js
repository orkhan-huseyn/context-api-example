import { useState } from 'react';

export default function AddPostForm({ onSubmit }) {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  function handleFormSubmit(event) {
    event.preventDefault();
    if (!title || !content) {
      return;
    }
    const post = { title, content };
    onSubmit(post);
    setTitle('');
    setContent('');
  }

  return (
    <form onSubmit={handleFormSubmit} className="mx-auto">
      <div className="mb-3">
        <label htmlFor="title">Post title</label>
        <input
          id="title"
          type="text"
          className="form-control"
          placeholder="Title of your post"
          onChange={(e) => setTitle(e.target.value)}
          value={title}
        />
      </div>
      <div className="mb-3">
        <label htmlFor="content">Post body</label>
        <textarea
          id="content"
          className="form-control"
          rows="3"
          placeholder="Content of your post"
          onChange={(e) => setContent(e.target.value)}
          value={content}
        ></textarea>
      </div>
      <button disabled={!title || !content} className="btn btn-primary">
        Submit
      </button>
    </form>
  );
}
